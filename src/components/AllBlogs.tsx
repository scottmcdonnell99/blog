import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { formatDistanceToNow } from 'date-fns';

interface Blog {
  id: number;
  title: string;
  content: string;
  created_at: string;
  username: string;
}

const AllBlogs: React.FC = () => {
  const [blogs, setBlogs] = useState<Blog[]>([]);
  const [loading, setLoading] = useState(true); // State for loading indicator

  useEffect(() => {
    const fetchBlogs = async () => {
      try {
        const response = await fetch('http://localhost:3000/posts');
        if (!response.ok) {
          throw new Error('Failed to fetch blogs');
        }
        const data = await response.json();
        // Sort the blogs by date in descending order
        const sortedBlogs = data.sort((a: Blog, b: Blog) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime());
        setBlogs(sortedBlogs);
      } catch (error) {
        console.error('Error fetching blogs:', error);
      } finally {
        // Set loading to false after fetching is complete (success or error)
        setLoading(false);
      }
    };

    fetchBlogs();
  }, []);

  const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    return formatDistanceToNow(date, { addSuffix: true });
  };
  

  const handleDelete = async (id: number) => {
    const confirmDelete = window.confirm('Are you sure you want to delete this blog post?');
    if (!confirmDelete) {
        return;
        }
    try {
      // Make DELETE request to delete the blog post
      const response = await fetch(`http://localhost:3000/posts/${id}`, {
        method: 'DELETE',
      });
      
      if (!response.ok) {
        throw new Error('Failed to delete blog post');
      }

      // Remove the deleted blog post from the state
      setBlogs(prevBlogs => prevBlogs.filter(blog => blog.id !== id));
    } catch (error) {
      console.error('Error deleting blog post:', error);
    }
  };

  return (
    <div className="max-w-md mx-auto">
      {loading ? (
        <div>Loading...</div>
      ) : (
        <ul className="m-0 p-0 space-y-4">
          {blogs.map((blog, index) => (
            <li key={blog.id} className="border border-gray-200 rounded-md p-4 shadow-sm">
              <div className="flex items-start">
                <div className="flex-shrink-0">
                  <img className="h-10 w-10 rounded-full" src="https://via.placeholder.com/50" alt={blog.username} />
                </div>
                <div className="ml-3">
                  <p className="username-link">{blog.username}</p>
                  <p className="text-sm text-gray-500">{formatDate(blog.created_at)}</p>
                </div>
              </div>
              <div className="mt-2">
                <p className="text-lg font-semibold">{blog.title}</p>
                <p className="text-gray-700">{blog.content}</p>
              </div>
              <div className="mt-2">
                <button onClick={() => handleDelete(blog.id)} className="text-sm font-medium text-red-600 hover:text-red-700 focus:outline-none">Delete</button>
              </div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default AllBlogs;
