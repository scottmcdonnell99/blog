import React, { useState, FormEvent } from 'react';

interface BlogFormProps {
  onSubmit: (title: string, content: string) => void;
}

const BlogForm: React.FC<BlogFormProps> = ({ onSubmit }) => {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
        const token = localStorage.getItem('token');
        if (!token) {
            // Handle case where token is missing
            console.error('Token is missing');
            return;
        }
        // Make POST request to create a new blog post
        const response = await fetch('http://localhost:3000/posts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Include the token in the Authorization header
            },
            body: JSON.stringify({ title, content })
        });

        if (!response.ok) {
            throw new Error('Failed to create blog post');
        }

        // Clear form fields
        setTitle('');
        setContent('');

        // Optionally, fetch updated blog posts after successful creation
        // const updatedBlogPosts = await response.json();
        // Update state or perform any necessary action with updatedBlogPosts
    } catch (error) {
        console.error(error);
        // Handle error appropriately, e.g., display error message to the user
    }
};


return (
    <div className="max-w-md mx-auto">
      <form onSubmit={handleSubmit} className="bg-white border border-gray-300 rounded-md shadow-md p-4">
        <input
          type="text"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          required
          className="w-full mb-2 px-3 py-2 text-sm border-b border-gray-200 focus:outline-none"
          placeholder="Title"
        />
        <textarea
          value={content}
          onChange={(e) => setContent(e.target.value)}
          required
          className="w-full mb-2 px-3 py-2 text-sm border-b border-gray-200 focus:outline-none"
          placeholder="Content"
          rows={3}
        ></textarea>
        <div className="flex justify-between items-center">
          <button type="submit" className="px-4 py-2 text-sm font-semibold text-white bg-blue-500 rounded-md hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2">Post</button>
          <p className="text-sm text-gray-500">Remaining characters: {140 - content.length}</p>
        </div>
      </form>
    </div>
  );
};


export default BlogForm;
