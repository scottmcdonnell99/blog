import React, { useState, FormEvent } from 'react';

interface LoginFormProps {
  onLogin: () => void;
  onError: (error: string) => void;
}

const LoginForm: React.FC<LoginFormProps> = ({ onLogin, onError }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    
    try {
        const response = await fetch('http://localhost:3000/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username, password }),
        });


        if (response.ok) {
            const { token } = await response.json();
            localStorage.setItem('token', token);
            console.log('Login successful');

            onLogin(); // Notify parent component about successful login
        } else {
            const data = await response.json();
            console.log('Login failed:', data.message);
            onError(data.message); // Notify parent component about login failure
        }
    } catch (error) {
        console.error('Login error:', error);
        onError('An error occurred during login'); // Notify parent component about login error
    }
  };

  return (
    <form onSubmit={handleSubmit} className="max-w-md mx-auto mt-8 p-8 bg-white rounded-md shadow-md">
      <div className="mb-6">
        <label htmlFor="username" className="block text-lg font-medium text-gray-700">Username</label>
        <input
          type="text"
          id="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          required
          className="mt-2 block w-full border-gray-300 rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500 text-lg px-4 py-2"
          placeholder="Enter your username"
        />
      </div>
      <div className="mb-6">
        <label htmlFor="password" className="block text-lg font-medium text-gray-700">Password</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
          className="mt-2 block w-full border-gray-300 rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500 text-lg px-4 py-2"
          placeholder="Enter your password"
        />
      </div>
      <button type="submit" className="w-full px-6 py-3 text-lg font-medium text-white bg-blue-500 rounded-md hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">Login</button>
    </form>
  );
};

export default LoginForm;
