import React, { useState, FormEvent } from 'react';

interface SignupFormProps {
  onSignup: () => void;
  onError: (error: string) => void;
}

const SignupForm: React.FC<SignupFormProps> = ({ onSignup, onError }) => {
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      const response = await fetch('http://localhost:3000/signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, username, password }),
      });

      if (response.ok) {
        console.log('Signup successful');

        onSignup(); // Notify parent component about successful signup
      } else {
        const data = await response.json();
        console.log('Signup failed:', data.message);
        onError(data.message); // Notify parent component about signup failure
      }
    } catch (error) {
      console.error('Signup error:', error);
      onError('An error occurred during signup'); // Notify parent component about signup error
    }
  };

  return (
    <form onSubmit={handleSubmit} className="max-w-md mx-auto mt-8 p-8 bg-white rounded-md shadow-md">
      <div className="mb-6">
        <label htmlFor="username" className="block text-lg font-medium text-gray-700">Email</label>
        <input
          type="text"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
          className="mt-2 block w-full border-gray-300 rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500 text-lg px-4 py-2"
          placeholder="Enter your email"
        />
      </div>
      <div className="mb-6">
        <label htmlFor="username" className="block text-lg font-medium text-gray-700">Username</label>
        <input
          type="text"
          id="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          required
          className="mt-2 block w-full border-gray-300 rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500 text-lg px-4 py-2"
          placeholder="Enter your username"
        />
      </div>
      <div className="mb-6">
        <label htmlFor="password" className="block text-lg font-medium text-gray-700">Password</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
          className="mt-2 block w-full border-gray-300 rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500 text-lg px-4 py-2"
          placeholder="Enter your password"
        />
      </div>
      <button type="submit" className="w-full px-6 py-3 text-lg font-medium text-white bg-blue-600 rounded-md hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">Signup</button>
    </form>
  );
};

export default SignupForm;
