// Navbar.tsx
import React from 'react';
import { Link } from 'react-router-dom'; // Import Link from react-router-dom

interface NavbarProps {
  isLoggedIn: boolean;
  onLogout: () => void;
}

const Navbar: React.FC<NavbarProps> = ({ isLoggedIn, onLogout }) => {
  return (
    <nav className="absolute top-0 left-0 mt-4 ml-4 flex flex-col">
      {isLoggedIn && (
        <>
          <Link to="/home" className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded mb-2">
            Home
          </Link>
          <Link to="/profile" className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded mb-2">
            Profile
          </Link>
          <button className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded mb-2" onClick={onLogout}>
            Logout
          </button>
        </>
      )}
    </nav>
  );
};

export default Navbar;
