import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import BlogForm from './components/BlogForm';
import AllBlogs from './components/AllBlogs';
import LoginForm from './components/LoginForm';
import SignupForm from './components/SignupForm';
import Navbar from './components/NavBar';

const App: React.FC = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isSignupMode, setIsSignupMode] = useState(false);

  // Check localStorage for login status on initial render
  useEffect(() => {
    const storedLoginStatus = localStorage.getItem('isLoggedIn');
    if (storedLoginStatus === 'true') {
      setIsLoggedIn(true);
    }
  }, []);

  const handleLogin = () => {
    setIsLoggedIn(true);
    localStorage.setItem('isLoggedIn', 'true'); // Store login status in localStorage
  };

  const handleLogout = () => {
    setIsLoggedIn(false);
    localStorage.removeItem('isLoggedIn'); // Remove login status from localStorage
  };

  const toggleMode = () => {
    setIsSignupMode(prevMode => !prevMode);
  };

  return (
    <Router>
      <Routes>
      </Routes>
      <div className="flex justify-center items-center min-h-screen bg-gray-100">
        <Navbar isLoggedIn={isLoggedIn} onLogout={handleLogout} />
          {isLoggedIn ? (
            <div>
              <div>
                <BlogForm />
              </div>
              <div className="max-w-md mx-auto mt-8">
                <AllBlogs />
              </div>
            </div>
          ) : (
            <div>
              {isSignupMode ? (
                <SignupForm onSignup={handleLogin} />
              ) : (
                <LoginForm onLogin={handleLogin} />
              )}
              <p className="mt-2 text-sm text-gray-600">
                {isSignupMode ? "Already have an account?" : "Don't have an account?"}{" "}
                <button onClick={toggleMode} className="text-blue-500">
                  {isSignupMode ? "Login" : "Signup"}
                </button>
              </p>
            </div>
          )}
      </div>
    </Router>
  );
};

export default App;
