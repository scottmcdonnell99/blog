# Blog App

The Blog App is a web application designed to facilitate the creation, management, and sharing of blog posts among users. It provides a user-friendly interface for registering, logging in, creating, and viewing blog posts.

## Features

### User Registration and Login
- Users can register for a new account by providing necessary information such as username, email, and password.
- Existing users can log in using their credentials to access their account.

### Blog Post Creation
- Logged-in users can create new blog posts by filling out a form that includes a title and content.
- Users can also edit and delete their own blog posts.

### Viewing Blog Posts
- Users can browse through a list of all blog posts created by themselves and other users.
- They can view individual blog posts to read the full content.

## Technologies Used
- **Frontend:** The frontend of the application is built using TypeScript, Vite, and Tailwind CSS. It provides an intuitive and responsive user interface for interacting with the blog platform.
- **Backend:** The backend of the application is powered by Node.js and PostgreSQL. It handles user authentication, blog post creation, retrieval, and management.
- **Authentication:** User authentication is implemented using JWT (JSON Web Tokens) to ensure secure login and protect user data.
- **Database:** The application utilizes PostgreSQL as the database system to store user information, blog posts, comments, and other relevant data. This allows for efficient retrieval and management of content.

## Future Enhancements
- **Comments and Interaction:** Implement a feature for users to leave comments on blog posts, enabling interaction and discussion among users.
- **Search and Filtering:** Introduce search and filtering options to allow users to easily find relevant blog posts based on keywords, tags, or categories.
- **Social Sharing:** Enable users to share blog posts on social media platforms, increasing visibility and reach.
- **User Profiles:** Create user profiles where users can customize their preferences, view their activity, and connect with other users.

The Blog App aims to provide a seamless and enjoyable experience for both creators and readers of blog content, fostering a vibrant community of bloggers and enthusiasts.
