// App.ts

import express, { Express, Request, Response, NextFunction} from 'express';
import db from './db'
import bcrypt from 'bcrypt';
import { JwtPayload, verify } from 'jsonwebtoken';
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv';

const cors = require('cors');
const app: Express = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(cors()); // This enables CORS for all routes and origins

dotenv.config();

function generateToken(payload: any): string {
    return jwt.sign(payload, process.env.JWT_SECRET!, { expiresIn: '1h' });
}


app.get('/', (req: Request, res: Response) => {
  res.send('Hello World from TypeScript Express server!');
});

// API endpoint for user registration
app.post('/signup', async (req, res) => {
    const { username, email, password } = req.body;
  
    try {
      // Check if username or email already exists
      const result = await db.query('SELECT * FROM users WHERE username = $1 OR email = $2', [username, email]);
      if (result.rows.length > 0) {
        return res.status(400).json({ message: 'Username or email already exists' });
      }
  
      // Hash the password before storing it
      const hashedPassword = await bcrypt.hash(password, 10);
  
      // Insert the new user into the database
      await db.query('INSERT INTO users (username, email, password) VALUES ($1, $2, $3)', [username, email, hashedPassword]);
  
      // Return success response
      res.status(201).json({ message: 'User registered successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Server error' });
    }
  });

app.post('/login', async (req: Request, res: Response) => {
    const { username, password } = req.body;

    try {
        // Query the database for the user
        const result = await db.query('SELECT * FROM USERS WHERE username = $1', [username]);

        if (result.rows.length > 0) {
            const user = result.rows[0];

            // Compare the provided password with the stored hash
            const isMatch = await bcrypt.compare(password, user.password);

            if (isMatch) {
                // Generate JWT token
                const token = generateToken({ username: user.username });

                // Send JWT token in response
                res.json({ token });
            } else {
                res.status(401).json({ message: "Invalid credentials." });
            }
        } else {
            // Username not found
            res.status(401).json({ message: "Invalid credentials." });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Server error" });
    }
});


app.get('/posts', async (req: Request, res: Response) => {
    try {
        // Query all blog posts from the database
        const result = await db.query('SELECT * FROM blog_posts');

        // Extract the rows from the result
        const blogPosts = result.rows;

        res.json(blogPosts);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Failed to fetch blog posts" });
    }
});


app.post('/posts', async (req, res) => {
    const { title, content } = req.body;
    try {
        // Check if the Authorization header exists
        const authorizationHeader = req.headers.authorization;
        if (!authorizationHeader) {
            return res.status(401).json({ message: "Authorization header is missing" });
        }

        // Split the token and retrieve the actual token value
        const token = authorizationHeader.split(' ')[1]; // Using optional chaining

        // Decode the JWT token to retrieve the username
        const decodedToken = verify(token, process.env.JWT_SECRET!) as JwtPayload;
        const username = decodedToken.username;

        // Retrieve the user's ID from the database using the username
        const userResult = await db.query('SELECT id FROM users WHERE username = $1', [username]);
        const userId = userResult.rows[0].id;

        // Insert new blog post into the database with the user's ID
        await db.query('INSERT INTO blog_posts (title, content, username, user_id) VALUES ($1, $2, $3, $4)', [title, content, username, userId]);

        // Query all blog posts from the database
        const result = await db.query('SELECT * FROM blog_posts');

        // Extract the rows from the result
        const blogPosts = result.rows;

        res.json(blogPosts);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Failed to create new blog post" });
    }
});


app.delete('/posts/:id', async (req: Request, res: Response) => {
    const postId = req.params.id;

    try {
        // Delete the blog post from the database
        await db.query('DELETE FROM blog_posts WHERE id = $1', [postId]);

        res.status(204).send(); // Sending 204 (No Content) since the resource is deleted successfully
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Failed to delete the blog post" });
    }
});


db.connect()
  .then(() => {
    console.log('Connected to PostgreSQL database');
  })
  .catch((err) => {
    console.error('Error connecting to PostgreSQL database:', err);
  });

app.listen(port, () => {
  console.log(`Server is running at https://localhost:${port}`);
});
